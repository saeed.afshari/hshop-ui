import config from "react-global-configuration";
import {Observable} from "rxjs";
import {ProductCategoryProp} from "../models/product-category-prop";
import {ajax} from "rxjs/ajax";

export class CategoryService {
    static fetchCategoriesByLevel(level: number): Observable<ProductCategoryProp[]> {
        return ajax.getJSON<ProductCategoryProp[]>(`${config.get('categoriesByLevelAPI')}/${level}`);
    };

    static fetchAllCategories(): Observable<ProductCategoryProp[]> {
        return ajax.getJSON<ProductCategoryProp[]>(config.get('categoriesBaseAPI'));
    };

    static fetchCategoryById(id: string): Observable<ProductCategoryProp> {
        return ajax.getJSON<ProductCategoryProp>(`${config.get('categoriesBaseAPI')}/${id}`);
    };

    static updateCategory(category: ProductCategoryProp) {
        return ajax.put(`${config.get('categoriesBaseAPI')}/${category.id}`,
            category, {'Content-Type': 'application/json'});
    }

    static createCategory(category: ProductCategoryProp) {
        return ajax.post(`${config.get('categoriesBaseAPI')}`, category,
            {'Content-Type': 'application/json'});
    }

    static deleteCategory(id: string) {
        return ajax.delete(`${config.get('categoriesBaseAPI')}/${id}`,
            {'Content-Type': 'application/json'});
    }

    static fetchCategoriesByNameAndLevel(categoryName: string, level: number): Observable<ProductCategoryProp> {
        return ajax.getJSON<ProductCategoryProp>(`${config.get('categoryByNameAPI')}/${categoryName}/level/${level}`)
    }
}

export default CategoryService;
