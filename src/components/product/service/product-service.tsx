import config from "react-global-configuration";
import {Observable} from "rxjs";
import {ProductProp} from "../models/product-prop";
import {ajax} from "rxjs/ajax";

export class ProductService {

    static fetchAllProducts(): Observable<ProductProp[]> {
        return ajax.getJSON<ProductProp[]>(config.get('productBaseAPI'));
    };

    static fetchProductById(id: string): Observable<ProductProp> {
        return ajax.getJSON<ProductProp>(`${config.get('productBaseAPI')}/${id}`);
    };

    static update(product: ProductProp) {
        return ajax.put(`${config.get('productBaseAPI')}/${product.id}`,
            product, {'Content-Type': 'application/json'});
    }

    static create(product: ProductProp) {
        product.categoryId = product.category.id;
        return ajax.post(`${config.get('productBaseAPI')}`, product,
            {'Content-Type': 'application/json'});
    }

    static delete(id: string) {
        return ajax.delete(`${config.get('productBaseAPI')}/${id}`,
            {'Content-Type': 'application/json'});
    }
}

export default ProductService;
