import React from 'react';
import './product-item.css';
import {ProductProp} from "../../models/product-prop";
import {Card} from "react-bootstrap";

export class ProductItemComponent extends React.Component<ProductProp> {

    componentDidMount() {
    }

    render() {
        return <Card style={{ width: '15rem' }}>
            <Card.Img variant="top" src={this.props.picture}/>
            <Card.Body>
                <Card.Title>{this.props.title}</Card.Title>
                <Card.Text>
                    Some quick example text to build on the card title and make up the bulk of
                    the card's content.
                </Card.Text>
                <Card.Footer>
                    {this.props.price} {this.props.currency}
                </Card.Footer>
            </Card.Body>
        </Card>
    }
}

export default ProductItemComponent;
