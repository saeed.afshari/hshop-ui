import React from 'react';
import {render} from '@testing-library/react';
import ProductItemComponent from "./product-item";

test('ProductItemComponent should be truthy', async () => {
  const {container} = render(<ProductItemComponent/>);
  expect(container).toBeTruthy();
});
