import React from 'react';
import './product-category.css';
import {Accordion, Card} from "react-bootstrap";
import {ProductCategoryProp} from "../../models/product-category-prop";

export class ProductCategoryComponent extends React.Component<ProductCategoryProp> {

    render() {
        return <div>
            <h1 className="font-weight-light">{this.props.name}</h1>
            <Accordion defaultActiveKey={this.props.selected}>
                {this.props.subCategories.map((category) => (
                    <Card key={category.name}>
                        <Card.Header>
                            <Accordion.Toggle as={() => {
                                return <a href={ProductCategoryComponent.getFullLink(category)}>
                                    {category.name}
                                </a>
                            }} eventKey={category.name} key={category.name}>
                                {category.name}
                            </Accordion.Toggle>
                        </Card.Header>
                    </Card>))}
            </Accordion>
        </div>
    }

    private static getFullLink(category: ProductCategoryProp) {
        return `${window.location.origin}/category/${category.link}`;
    }
}

export default ProductCategoryComponent;
