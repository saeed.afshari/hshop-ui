import React from 'react';
import {render} from '@testing-library/react';
import ProductCategoryComponent from "./product-category";

test('ProductCategoryComponent should be truthy', async () => {
  const {container} = render(<ProductCategoryComponent selected={''} link={''} name={''} subCategories={[]}/>);
  expect(container).toBeTruthy();
});
