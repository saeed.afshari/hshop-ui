import {DefaultProductCategoryProp, ProductCategoryProp} from "./product-category-prop";

export interface ProductProp {
    id?: string;
    title: string;
    price: number;
    currency: string;
    picture: string;
    category: ProductCategoryProp;
    categoryId?: string;
}

export const DefaultProductProp: ProductProp = {
    title: '',
    price: 0,
    currency: 'EUR',
    picture: '',
    category: {...DefaultProductCategoryProp},
    categoryId: ''
}
