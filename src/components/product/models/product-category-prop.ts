import {ProductProp} from "./product-prop";

export interface ProductCategoryProp {
    id?: string;
    name: string;
    link: string;
    level: number;
    selected: string;
    subCategories: ProductCategoryProp[];
    categoryProducts?: ProductProp[];
    parentCategoryId?: string;
    parentCategoryName?: string;
}

export const DefaultProductCategoryProp: ProductCategoryProp = {
    id: "",
    name: "",
    level: 0,
    link: "",
    selected: "",
    subCategories: []
}
