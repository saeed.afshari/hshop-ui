import {BehaviorSubject, EMPTY} from "rxjs";
import {DefaultProductCategoryProp, ProductCategoryProp} from "../models/product-category-prop";
import CategoryService from "../service/category-service";
import {catchError} from "rxjs/operators";

export class CategoryFacadeService {
    static error$: BehaviorSubject<any> = new BehaviorSubject<any>(null);
    static levelOneCategories$: BehaviorSubject<ProductCategoryProp[]> = new BehaviorSubject<ProductCategoryProp[]>([]);
    static categoryByNameAndLevel$: BehaviorSubject<ProductCategoryProp> =
        new BehaviorSubject<ProductCategoryProp>({...DefaultProductCategoryProp});
    static allCategories$: BehaviorSubject<ProductCategoryProp[]> = new BehaviorSubject<ProductCategoryProp[]>([]);
    static selectedCategory$: BehaviorSubject<ProductCategoryProp> =
        new BehaviorSubject<ProductCategoryProp>({...DefaultProductCategoryProp});
    static categoryUpdated$: BehaviorSubject<any> = new BehaviorSubject<any>({...DefaultProductCategoryProp});
    static categoryCreated$: BehaviorSubject<any> = new BehaviorSubject<any>({...DefaultProductCategoryProp});
    static categoryDeleting$: BehaviorSubject<string> = new BehaviorSubject<any>(null);
    static categoryDeleted$: BehaviorSubject<any> = new BehaviorSubject<any>(null);

    static dispatchGetCategoriesByLevel(level: number) {
        CategoryService.fetchCategoriesByLevel(level)
            .pipe(catchError(err => {
                this.error$.next(err);
                return EMPTY;
            })).subscribe(categories => {
            this.levelOneCategories$.next(categories);
        })
    }

    static dispatchGetAllCategories() {
        CategoryService.fetchAllCategories()
            .pipe(catchError(err => {
                this.error$.next(err);
                return EMPTY;
            })).subscribe(categories => {
            this.allCategories$.next(categories);
        })
    }

    static dispatchGetCategoryById(id: string) {
        CategoryService.fetchCategoryById(id)
            .pipe(catchError(err => {
                this.error$.next(err);
                return EMPTY;
            })).subscribe(category => {
            this.selectedCategory$.next(category);
        })
    }

    static dispatchUpdateCategory(category: ProductCategoryProp) {
        CategoryService.updateCategory(category)
            .pipe(catchError(err => {
                this.error$.next(err);
                return EMPTY;
            })).subscribe(category => {
            this.categoryUpdated$.next(category.response);
        })
    }

    static dispatchCreateCategory(category: ProductCategoryProp) {
        CategoryService.createCategory(category)
            .pipe(catchError(err => {
                this.error$.next(err);
                return EMPTY;
            })).subscribe(category => {
            this.categoryCreated$.next(category.response);
        })
    }

    static dispatchDeletingCategory(id: string) {
        this.categoryDeleting$.next(id);
    }

    static dispatchDeleteCategory(id: string) {
        CategoryService.deleteCategory(id)
            .pipe(catchError(err => {
                this.error$.next(err);
                return EMPTY;
            })).subscribe(() => {
            this.categoryDeleted$.next(id);
        })
    }

    static dispatchGetCategoriesByNameAndLevel(categoryName: string, level: number) {
        CategoryService.fetchCategoriesByNameAndLevel(categoryName, level)
            .pipe(catchError(err => {
                this.error$.next(err);
                return EMPTY;
            })).subscribe(category => {
            this.categoryByNameAndLevel$.next(category);
        })
    }
}

export default CategoryFacadeService;
