import {BehaviorSubject, EMPTY} from "rxjs";
import {catchError} from "rxjs/operators";
import {DefaultProductProp, ProductProp} from "../models/product-prop";
import ProductService from "../service/product-service";

export class ProductFacadeService {
    static error$: BehaviorSubject<any> = new BehaviorSubject<any>(null);
    static allProducts$: BehaviorSubject<ProductProp[]> = new BehaviorSubject<ProductProp[]>([]);
    static selectedProduct$: BehaviorSubject<ProductProp> =
        new BehaviorSubject<ProductProp>({...DefaultProductProp});
    static productUpdated$: BehaviorSubject<any> = new BehaviorSubject<any>({...DefaultProductProp});
    static productCreated$: BehaviorSubject<any> = new BehaviorSubject<any>({...DefaultProductProp});
    static productDeleting$: BehaviorSubject<string> = new BehaviorSubject<any>(null);
    static productDeleted$: BehaviorSubject<any> = new BehaviorSubject<any>(null);

    static dispatchGetAllProducts() {
        ProductService.fetchAllProducts()
            .pipe(catchError(err => {
                this.error$.next(err);
                return EMPTY;
            })).subscribe(products => {
            this.allProducts$.next(products);
        })
    }

    static dispatchGetProductById(id: string) {
        ProductService.fetchProductById(id)
            .pipe(catchError(err => {
                this.error$.next(err);
                return EMPTY;
            })).subscribe(product => {
            this.selectedProduct$.next(product);
        })
    }

    static dispatchUpdateProduct(product: ProductProp) {
        ProductService.update(product)
            .pipe(catchError(err => {
                this.error$.next(err);
                return EMPTY;
            })).subscribe(product => {
            this.productUpdated$.next(product.response);
        })
    }

    static dispatchCreateProduct(product: ProductProp) {
        ProductService.create(product)
            .pipe(catchError(err => {
                this.error$.next(err);
                return EMPTY;
            })).subscribe(product => {
            this.productCreated$.next(product.response);
        })
    }

    static dispatchDeletingProduct(id: string) {
        this.productDeleting$.next(id);
    }

    static dispatchDeleteProduct(id: string) {
        ProductService.delete(id)
            .pipe(catchError(err => {
                this.error$.next(err);
                return EMPTY;
            })).subscribe(() => {
            this.productDeleted$.next(id);
        })
    }

    static dispatchError(error: { response: { message: string } }) {
        this.error$.next(error);
    }
}

export default ProductFacadeService;
