import React from 'react';
import {render} from '@testing-library/react';
import {CategoryAddFormComponent} from "./category-add-form";

test('ProductCategoryComponent should be truthy', async () => {
  const {container} = render(<CategoryAddFormComponent selected={''} link={''} name={''} subCategories={[]}/>);
  expect(container).toBeTruthy();
});
