import React from 'react';
import './category-add-form.css';
import {Button, Form} from "react-bootstrap";
import {DefaultProductCategoryProp, ProductCategoryProp} from "../../models/product-category-prop";
import CategoryFacadeService from "../../facades/category-facade-service";

export class CategoryAddFormComponent extends React.Component<any, any> {

    constructor(props: any) {
        super(props);

        this.handleChange = this.handleChange.bind(this);
        this.handleSubmit = this.handleSubmit.bind(this);
    }

    private fetchCategory(id: string) {
        CategoryFacadeService.dispatchGetCategoryById(id);

        CategoryFacadeService.selectedCategory$.subscribe(category => {
            this.setState({
                selectedProduct: category,
                categories: this.state ? this.state.categories : [],
                validated: false
            });
        });
    }

    private fetchCategories() {
        CategoryFacadeService.dispatchGetAllCategories();

        CategoryFacadeService.allCategories$.subscribe(categories => {
            if (this.state && this.state.selectedProduct) {
                const index = categories.findIndex(item => item.id === this.state.selectedProduct.id);
                if (index >= 0) {
                    categories.splice(index, 1);
                }
            }
            this.setState({
                selectedProduct: this.state ? this.state.selectedProduct : {...DefaultProductCategoryProp},
                categories: categories,
                validated: false
            });
        });
    }

    componentDidMount() {
        const id = this.props.match.params.id;
        if (id) {
            this.fetchCategory(id)
        }

        this.fetchCategories();
    }

    handleSubmit(event: any) {
        event.preventDefault();

        const form = event.currentTarget;
        if (form.checkValidity() === false) {
            event.stopPropagation();
            this.setState({...this.state, validated: true});
            return;
        }

        const selectedCategory = this.state.selectedProduct;
        const parent = this.state.categories.find((item: ProductCategoryProp) => item.id === selectedCategory.parentCategoryId);
        if (parent) {
            selectedCategory.level = parent.level + 1;
        } else {
            selectedCategory.level = 1;
        }
        if (selectedCategory.id) {
            CategoryFacadeService.dispatchUpdateCategory(selectedCategory);
        } else {
            CategoryFacadeService.dispatchCreateCategory(selectedCategory);
        }
    }

    handleChange(event: any) {
        const state = {...this.state}
        state.selectedProduct[event.target.name] = event.target.value;
        this.setState(state);
    }

    render() {
        if (!this.state || !this.state.selectedProduct) {
            return <div/>;
        }
        if (this.state.selectedProduct.id && !this.state.selectedProduct.name) {
            return <div/>;
        }
        if (!this.state.categories) {
            return <div/>;
        }

        const parentCategoryId = this.state.selectedProduct.parentCategoryId ? this.state.selectedProduct.parentCategoryId : 1;

        return <Form noValidate validated={this.state.validated} onSubmit={this.handleSubmit}>
            <Form.Group controlId="name">
                <Form.Label>Name</Form.Label>
                <Form.Control name="name" value={this.state.selectedProduct.name} required
                              onChange={this.handleChange} type="text" placeholder="Enter category name"/>
            </Form.Group>

            <Form.Group controlId="parentCategory">
                <Form.Label>Parent group</Form.Label>
                <Form.Control required onChange={this.handleChange} as="select" name="parentCategoryId"
                              value={parentCategoryId}>
                    <option value={1} key={1}>No parent</option>
                    {this.state.categories.map((item: ProductCategoryProp) => (
                        <option value={item.id} key={item.id}>{item.name} - {item.level}</option>
                    ))}
                </Form.Control>
            </Form.Group>

            <Button variant="primary" type="submit">
                Submit
            </Button>
        </Form>
    }
}

export default CategoryAddFormComponent;
