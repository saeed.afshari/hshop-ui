import React from 'react';
import './product-add-form.css';
import {Button, Card, Col, Form, Row} from "react-bootstrap";
import {ProductCategoryProp} from "../../models/product-category-prop";
import CategoryFacadeService from "../../facades/category-facade-service";
import ProductFacadeService from "../../facades/product-facade-service";
import UploaderComponent from "../../../upload/dump-components/uploader";
import {DefaultProductProp} from "../../models/product-prop";
import UploadFacadeService from "../../../upload/facades/upload-facade-service";

export class ProductAddFormComponent extends React.Component<any, any> {

    constructor(props: any) {
        super(props);

        this.handleChange = this.handleChange.bind(this);
        this.handleSubmit = this.handleSubmit.bind(this);
    }

    private fetchProduct(id: string) {
        ProductFacadeService.dispatchGetProductById(id);

        ProductFacadeService.selectedProduct$.subscribe(product => {
            if (product.id) {
                let state = {
                    ...this.state,
                    selectedProduct: product,
                };
                this.setState(state);
            }
        });
    }

    private fetchCategories() {
        CategoryFacadeService.dispatchGetAllCategories();

        CategoryFacadeService.allCategories$.subscribe(categories => {
            if (categories.length > 0) {
                this.setState({
                    ...this.state,
                    categories: categories
                });
            }
        });
    }

    componentDidMount() {
        this.setState({
            validated: false,
            categories: [],
            selectedProduct: DefaultProductProp
        });

        const id = this.props.match.params.id;
        if (id) {
            this.fetchProduct(id)
        }

        this.fetchCategories();

        this.registerToFileUpload();
    }

    handleSubmit(event: any) {
        event.preventDefault();

        const form = event.currentTarget;
        if (form.checkValidity() === false) {
            event.stopPropagation();
            this.setState({...this.state, validated: true});
            return;
        }

        const selectedProduct = this.state.selectedProduct;
        if (!selectedProduct.picture) {
            ProductFacadeService.dispatchError({
                response: {
                    message: 'Picture is not uploaded! Please upload a picture to continue.'
                }
            });
            return;
        }

        if (!this.state.selectedProduct.categoryId) {
            selectedProduct.categoryId = selectedProduct.category.id;
        }

        if (selectedProduct.id) {
            ProductFacadeService.dispatchUpdateProduct(selectedProduct);
        } else {
            ProductFacadeService.dispatchCreateProduct(selectedProduct);
        }
    }

    handleChange(event: any) {
        const state = {...this.state}
        if (event.target.name === 'categoryId') {
            state.selectedProduct.category.id = event.target.value;
        } else {
            state.selectedProduct[event.target.name] = event.target.value;
        }
        this.setState(state);
    }

    render() {
        if (!this.state) {
            return <></>;
        }
        if (this.props.match.params.id) {
            if (!this.state.selectedProduct || !this.state.categories) {
                return <></>;
            }
        } else {
            if (this.state.categories.length === 0) {
                return <></>;
            }
        }
        // alert(this.state.selectedProduct.categoryId)
        return (
            <>
                <Row className="mb-5 text-center align-items-center">
                    <Col>
                        <h3>Create a product</h3>
                    </Col>
                </Row>

                <Row className="mb-5">
                    <Col>
                        <UploaderComponent title="Upload a picture"/>
                    </Col>
                </Row>

                <Row className="mb-5">
                    <Col>
                        <Card.Img variant="top" style={{width: '25rem', height: '15rem'}}
                                  src={this.state.selectedProduct.picture ? this.state.selectedProduct.picture : '286x180.svg'}
                                  alt={this.state.selectedProduct.title}
                        />
                    </Col>
                </Row>

                <Form noValidate validated={this.state.validated} onSubmit={this.handleSubmit}>

                    <Form.Group controlId="title">
                        <Form.Label>Title</Form.Label>
                        <Form.Control name="title" value={this.state.selectedProduct.title} required
                                      onChange={this.handleChange} type="text" placeholder="Enter title"/>
                    </Form.Group>

                    <Form.Group controlId="price">
                        <Form.Label>Price</Form.Label>
                        <Form.Control name="price" value={this.state.selectedProduct.price} required
                                      onChange={this.handleChange} type="number" placeholder="Enter price"/>
                    </Form.Group>

                    <Form.Group controlId="categoryId">
                        <Form.Label>Category</Form.Label>
                        <Form.Control required onChange={this.handleChange} as="select" name="categoryId"
                                      value={this.state.selectedProduct.category.id}>
                            <option value=''>Select a category ...</option>
                            {this.state.categories.map((item: ProductCategoryProp) => (
                                <option value={item.id} key={item.id}>{item.name} - {item.level}</option>
                            ))}
                        </Form.Control>
                    </Form.Group>

                    <Button variant="primary" type="submit">
                        Submit
                    </Button>
                </Form>
            </>
        )
    }

    private registerToFileUpload() {
        UploadFacadeService.fileUploaded$.subscribe(file => {
            if (file) {
                const selectedProduct = this.state.selectedProduct;
                selectedProduct.picture = file.fileDownloadUri;
                this.setState({
                    ...this.state,
                    selectedProduct: selectedProduct
                })
            }
        });
    }
}

export default ProductAddFormComponent;
