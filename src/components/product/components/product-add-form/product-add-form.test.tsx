import React from 'react';
import {render} from '@testing-library/react';
import ProductAddFormComponent from "./product-add-form";

test('ProductCategoryComponent should be truthy', async () => {
  const {container} = render(<ProductAddFormComponent/>);
  expect(container).toBeTruthy();
});
