import React from 'react';
import {render} from '@testing-library/react';
import ProductViewComponent from "./product-view";

test('ProductViewComponent should be truthy', async () => {
  const {container} = render(<ProductViewComponent match={{params: []}}/>);
  expect(container).toBeTruthy();
});
