import React from 'react';
import './product-view.css';
import {Col, Row} from "react-bootstrap";
import BreadcrumbComponent from "../../../breadcrumb/breadcrumb";
import {BreadcrumbItem} from "../../../models/breadcrumb-prop";
import ProductCategoryComponent from "../../dumb-components/product-category/product-category";
import CategoryFacadeService from "../../facades/category-facade-service";
import {ProductCategoryProp} from "../../models/product-category-prop";
import {ProductProp} from "../../models/product-prop";
import ProductItemComponent from "../../dumb-components/product-item/product-item";

export class ProductViewComponent extends React.Component<any, any> {

    constructor(props: any,
                private breadcrumbItems: BreadcrumbItem[]) {
        super(props);
    }

    private createBreadcrumbItems(): BreadcrumbItem[] {
        const items: BreadcrumbItem[] = [];
        const entries = Object.entries(this.props.match.params);
        let link = '/category';
        let i = -1;
        for (let [_, value] of entries) {
            if (!value) {
                break;
            }
            link += `/${value}`;
            items.push({
                title: `${value}`,
                active: false,
                link: link
            });
            i++;
        }
        if (items.length > 0) {
            items[i].active = true;
        }
        return items;
    }

    private getParentCategory(): BreadcrumbItem {
        if (this.breadcrumbItems.length === 0) {
            return {title: '', active: false, link: ''};
        }
        return this.breadcrumbItems[this.breadcrumbItems.length - 1];
    }

    fetchCategories(categoryName: string, level: number) {
        const selected = (new URLSearchParams(window.location.search)).get("selected");
        CategoryFacadeService.dispatchGetCategoriesByNameAndLevel(categoryName, level);
        CategoryFacadeService.categoryByNameAndLevel$.subscribe(category => {
            if (!category.id) {
                return;
            }
            if(selected) {
                const selectedCategory = category.subCategories.find(item => item.name === selected);
                this.setState({
                    category: category,
                    selected: selected,
                    products: selectedCategory ? selectedCategory.categoryProducts : []
                });
            } else{
                this.setState({
                    category: category,
                    selected: selected,
                    products: category.categoryProducts
                });
            }
        });
    }

    componentDidMount() {
        this.breadcrumbItems = this.createBreadcrumbItems();
        this.fetchCategories(this.getParentCategory().title, this.breadcrumbItems.length);
    }

    render() {
        if (!this.state) {
            return <div></div>
        }
        const subCategories = this.state.category.subCategories ? this.state.category.subCategories : [];

        return <Row>
            <Col xs={12} sm={5} md={3} className="product-category d-none d-sm-block">
                <ProductCategoryComponent name={this.getParentCategory().title} link={this.state.category.link}
                                          level={this.state.category.level}
                                          subCategories={subCategories}
                                          selected={this.getSelectedCategory(subCategories)}>

                </ProductCategoryComponent>
            </Col>
            <Col xs={12} sm={7} md={9}>
                <Row>
                    <Col>
                        <BreadcrumbComponent items={this.breadcrumbItems}/>
                    </Col>
                </Row>
                <Row>
                    {this.state.products ? this.state.products.map((product: ProductProp) => (
                        <Col sm={12} md={6} xl={4} className="mb-5">
                            <ProductItemComponent title={product.title} picture={product.picture}
                                                  category={this.state.category} currency={product.currency}
                                                  price={product.price} id={product.id} key={product.id}/>
                        </Col>
                    )) : <></>}
                </Row>
            </Col>
        </Row>
    }

    private getSelectedCategory(subCategories: ProductCategoryProp[]) {
        if (this.state.selected) {
            return this.state.selected;
        } else {
            return subCategories.length > 0 ? subCategories[0].name : '';
        }
    }
}

export default ProductViewComponent;
