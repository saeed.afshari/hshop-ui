import React from 'react';
import {render} from '@testing-library/react';
import {CategoryListComponent} from "./category-list";

test('ProductViewComponent should be truthy', async () => {
  const {container} = render(<CategoryListComponent/>);
  expect(container).toBeTruthy();
});
