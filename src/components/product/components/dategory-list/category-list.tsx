import React from 'react';
import './category-list.css';
import {Col, Row, Table} from "react-bootstrap";
import {FaPlusSquare, FaTrash} from 'react-icons/fa';
import CategoryFacadeService from "../../facades/category-facade-service";

export class CategoryListComponent extends React.Component<any, any> {

    fetchCategories() {
        CategoryFacadeService.dispatchGetAllCategories();

        CategoryFacadeService.allCategories$.subscribe(categories => {
            this.setState({
                categories: categories,
            });
        });
    }

    componentDidMount() {
        this.fetchCategories();
    }

    render() {
        if (!this.state) {
            return <></>
        }
        return (
            <div>
                <Row className="toolbar">
                    <Col className="text-right">
                        <a href='/category-add'>
                            <FaPlusSquare color="green" size={'2.5em'}/>
                        </a>
                    </Col>
                </Row>
                <Row>
                    <Col>
                        <Table bordered hover>
                            <thead>
                            <tr>
                                <th>Name</th>
                                <th>Level</th>
                                <th>Parent category</th>
                                <th>Actions</th>
                            </tr>
                            </thead>
                            <tbody>
                            {this.state.categories.map((category: any) => (
                                <tr key={category.id}>
                                    <td><a href={'category-add/' + category.id}>{category.name}</a></td>
                                    <td>{category.level}</td>
                                    <td>{category.parentCategoryName ? category.parentCategoryName : '---'}</td>
                                    <td>
                                        <a href="#"
                                           onClick={() => CategoryFacadeService.dispatchDeletingCategory(category.id)}>
                                            <FaTrash color="red" size={'1em'}/>
                                        </a>
                                    </td>
                                </tr>
                            ))}
                            </tbody>
                        </Table>
                    </Col>
                </Row>
            </div>
        )
    }
}

export default CategoryListComponent;
