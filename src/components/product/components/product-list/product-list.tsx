import React from 'react';
import './product-list.css';
import {Col, Row, Table} from "react-bootstrap";
import {FaPlusSquare, FaTrash} from 'react-icons/fa';
import ProductFacadeService from "../../facades/product-facade-service";

export class ProductListComponent extends React.Component<any, any> {

    fetchProducts() {
        ProductFacadeService.dispatchGetAllProducts();

        ProductFacadeService.allProducts$.subscribe(products => {
            this.setState({
                products: products,
            });
        });
    }

    componentDidMount() {
        this.fetchProducts();
    }

    render() {
        if (!this.state) {
            return <></>
        }
        return (
            <div>
                <Row className="toolbar">
                    <Col className="text-right">
                        <a href='/product-add'>
                            <FaPlusSquare color="green" size={'2.5em'}/>
                        </a>
                    </Col>
                </Row>
                <Row>
                    <Col>
                        <Table bordered hover>
                            <thead>
                            <tr>
                                <th>Title</th>
                                <th>Category link</th>
                                <th>Price</th>
                                <th>Picture</th>
                                <th>Actions</th>
                            </tr>
                            </thead>
                            <tbody>
                            {this.state.products.map((product: any) => (
                                <tr key={product.id}>
                                    <td><a href={'product-add/' + product.id}>{product.title}</a></td>
                                    <td>{product.category.link}</td>
                                    <td>{product.price}</td>
                                    <td><img style={{width: '4rem'}} src={product.picture} alt={product.title}/></td>
                                    <td>
                                        <a href="#"
                                           onClick={() => ProductFacadeService.dispatchDeletingProduct(product.id)}>
                                            <FaTrash color="red" size={'1em'}/>
                                        </a>
                                    </td>
                                </tr>
                            ))}
                            </tbody>
                        </Table>
                    </Col>
                </Row>
            </div>
        )
    }
}

export default ProductListComponent;
