import React from 'react';
import {render} from '@testing-library/react';
import ProductListComponent from "./product-list";

test('ProductViewComponent should be truthy', async () => {
  const {container} = render(<ProductListComponent/>);
  expect(container).toBeTruthy();
});
