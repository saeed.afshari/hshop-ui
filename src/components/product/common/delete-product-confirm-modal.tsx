import React from 'react';
import {Button, Modal} from "react-bootstrap";
import ProductFacadeService from "../facades/product-facade-service";

export class DeleteProductConfirmModalComponent extends React.Component<any, any> {

    componentDidMount() {
        this.setState({show: false, id: null});

        ProductFacadeService.productDeleting$.subscribe(id => {
            if (id) {
                this.setState({show: true, id: id});
            }
        });
    }

    cancel() {
        this.setState({show: false, id: null});
    }

    confirm() {
        ProductFacadeService.dispatchDeleteProduct(this.state.id)
        this.setState({show: false, id: null});
    }

    render() {
        if (!this.state) {
            return <></>
        }
        return (
            <Modal show={this.state.show} onHide={() => this.cancel()}>
                <Modal.Header closeButton>
                    <Modal.Title>Delete!</Modal.Title>
                </Modal.Header>
                <Modal.Body>You are deleting a product! Are you sure?</Modal.Body>
                <Modal.Footer>
                    <Button variant="secondary" onClick={() => this.cancel()}>
                        Close
                    </Button>
                    <Button variant="danger" onClick={() => this.confirm()}>
                        Yes
                    </Button>
                </Modal.Footer>
            </Modal>
        )
    }
}

export default DeleteProductConfirmModalComponent;
