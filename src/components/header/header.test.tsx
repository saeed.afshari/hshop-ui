import React from 'react';
import {render} from '@testing-library/react';
import HeaderComponent from "./header";

test('HeaderComponent should be truthy', async () => {
  const {container} = render(<HeaderComponent/>);
  expect(container).toBeTruthy();
});
