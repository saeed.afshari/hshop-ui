import React from 'react';
import './header.css';
import Navbar from "react-bootstrap/Navbar";
import Nav from "react-bootstrap/Nav";
import logo from './logo.svg';
import CategoryFacadeService from "../product/facades/category-facade-service";

export class HeaderComponent extends React.Component<any, any> {
    fetchCategories() {
        CategoryFacadeService.dispatchGetCategoriesByLevel(1);

        CategoryFacadeService.levelOneCategories$.subscribe(categories => {
            this.setState({
                categories: categories
            });
        });
    }

    componentDidMount() {
        this.fetchCategories();
    }

    render() {
        if (!this.state) {
            return <div></div>
        }
        return <div className="header">
            <Navbar bg="light" expand="lg">
                <Navbar.Brand href={window.location.origin}><img src={logo} className="brand-logo"
                                                                 alt="hse24"/></Navbar.Brand>
                <Navbar.Toggle aria-controls="basic-navbar-nav"/>
                <Navbar.Collapse id="basic-navbar-nav">
                    <Nav className="mr-auto">
                        {this.state.categories.map((item: any) => (
                                <a className="nav-link" href={window.location.origin + `/category/${item.name}`}
                                   key={item.id}>{item.name}</a>
                            )
                        )}
                    </Nav>
                    <Nav className="font-weight-bold">
                        <a className="nav-link mr-sm-1" href={window.location.origin + `/category-list`}
                           key={'categories'}>Categories</a>

                        <a className="nav-link mr-sm-1" href={window.location.origin + `/product-list`}
                           key={'products'}>Products</a>
                    </Nav>
                </Navbar.Collapse>
            </Navbar>
        </div>
    }
}

export default HeaderComponent;
