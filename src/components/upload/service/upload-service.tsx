import config from "react-global-configuration";
import {ajax} from "rxjs/ajax";

export class UploadService {
    static upload(file: any) {
        const formData = new FormData();
        formData.append('file', file);
        return ajax({
            url: `${config.get('uploadBaseAPI')}`,
            body: formData,
            method: 'POST'
        });
    }
}

export default UploadService;
