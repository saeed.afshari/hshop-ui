import React from 'react';
import './uploader.css';
import {UploadProp} from "../model/upload-prop";
import UploadFacadeService from "../facades/upload-facade-service";

export class UploaderComponent extends React.Component<UploadProp, any> {
    constructor(props: UploadProp) {
        super(props);
        this.state = {
            file: null
        }
        this.onFormSubmit = this.onFormSubmit.bind(this)
        this.onChange = this.onChange.bind(this)
    }

    onFormSubmit(e: any) {
        e.preventDefault()
        UploadFacadeService.dispatchUploadFile(this.state.file);
    }

    onChange(e: any) {
        this.setState({file: e.target.files[0]})
        console.log(e.target.files[0])
    }

    render() {
        return (
            <form onSubmit={this.onFormSubmit} className="form-inline">
                <input type="file" onChange={this.onChange}/>
                <button className="btn-success btn ml-auto" type="submit">Upload</button>
            </form>
        )
    }
}

export default UploaderComponent;
