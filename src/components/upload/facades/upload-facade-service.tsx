import {BehaviorSubject, EMPTY} from "rxjs";
import {catchError} from "rxjs/operators";
import UploadService from "../service/upload-service";

export class UploadFacadeService {
    static error$: BehaviorSubject<any> = new BehaviorSubject<any>(null);
    static fileUploaded$: BehaviorSubject<any> = new BehaviorSubject<any>(null);

    static dispatchUploadFile(file: any) {
        UploadService.upload(file)
            .pipe(catchError(err => {
                this.error$.next(err);
                return EMPTY;
            })).subscribe(product => {
            this.fileUploaded$.next(product.response);
        })
    }
}

export default UploadFacadeService;
