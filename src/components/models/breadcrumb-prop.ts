export interface BreadcrumbProp {
    items: BreadcrumbItem[]
}

export interface BreadcrumbItem {
    link: string;
    title: string;
    active: boolean;
}
