import React from 'react';
import {render} from '@testing-library/react';
import App from "./app";

test('AppComponent should be truthy', () => {
  const {container} = render(<App/>);
  expect(container).toBeTruthy();
});
