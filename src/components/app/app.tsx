import React from 'react';
import './app.css';
import HeaderComponent from "../header/header";
import {Container} from "react-bootstrap";
import {BrowserRouter as Router, Route, Switch} from 'react-router-dom';
import ProductViewComponent from "../product/components/product-view/product-view";
import CategoryListComponent from "../product/components/dategory-list/category-list";
import CategoryAddFormComponent from "../product/components/category-add-form/category-add-form";
import toaster from "toasted-notes";
import CategoryFacadeService from "../product/facades/category-facade-service";
import {DefaultProductCategoryProp} from "../product/models/product-category-prop";
import {MessageOptionalOptions} from "toasted-notes/lib/ToastManager";
import ProductListComponent from "../product/components/product-list/product-list";
import ProductFacadeService from "../product/facades/product-facade-service";
import DeleteConfirmModalComponent from "../product/common/delete-confirm-modal";
import DeleteProductConfirmModalComponent from "../product/common/delete-product-confirm-modal";
import {DefaultProductProp} from "../product/models/product-prop";
import ProductAddFormComponent from "../product/components/product-add-form/product-add-form";
import UploadFacadeService from "../upload/facades/upload-facade-service";

export class App extends React.Component {

    private toastrInforOptions: MessageOptionalOptions = {
        type: 'success',
        position: "top-right"
    };

    componentDidMount() {
        this.registerCategoryToastrs();
        this.registerProductToastrs();
        this.registerUploadToastrs();
    }

    render() {
        return <div>
            <HeaderComponent/>
            <Container className="main-container">
                <Router>
                    <Switch>
                        <Route path="/category-list" component={CategoryListComponent}/>
                        <Route path="/category-add/:id?" component={CategoryAddFormComponent}/>

                        <Route path="/product-list" component={ProductListComponent}/>
                        <Route path="/product-add/:id?" component={ProductAddFormComponent}/>

                        <Route path="/category/:cat1/:cat2?/:cat3?/:cat4?/:cat5?/:cat6?"
                               component={ProductViewComponent}/>
                    </Switch>
                </Router>
            </Container>

            <DeleteConfirmModalComponent/>
            <DeleteProductConfirmModalComponent/>
        </div>
    }

    private registerCategoryToastrs() {
        const infoOptions = this.toastrInforOptions;
        CategoryFacadeService.categoryCreated$.subscribe(category => {
            if (category.name !== DefaultProductCategoryProp.name) {
                window.location.href = `${window.location.href}/${category.id}`;
            }
        });

        CategoryFacadeService.categoryUpdated$.subscribe(category => {
            if (category.name !== DefaultProductCategoryProp.name) {
                toaster.notify(`Category ${category.name} updated successfully!`, infoOptions);
                CategoryFacadeService.dispatchGetCategoriesByLevel(1);
            }
        });

        CategoryFacadeService.categoryDeleted$.subscribe((id) => {
            if (id) {
                toaster.notify(`Category deleted successfully!`, infoOptions);
                CategoryFacadeService.dispatchGetCategoriesByLevel(1);
                CategoryFacadeService.dispatchGetAllCategories();
            }
        });

        CategoryFacadeService.error$.subscribe(error => {
            App.handleError(error);
        });
    }

    private static handleError(error: any) {
        const toastrErrorOptions: MessageOptionalOptions = {
            type: 'error',
            position: "top-right"
        };
        if (error) {
            toaster.closeAll();
            if (error.response) {
                toaster.notify(`${error.response.message}`, toastrErrorOptions);
            } else {
                toaster.notify(`${error.message}: ${error.request.url} is not available!`, toastrErrorOptions);
            }
        }
    }

    private registerProductToastrs() {
        const infoOptions = this.toastrInforOptions;

        ProductFacadeService.productCreated$.subscribe(product => {
            if (product.id && product.id !== DefaultProductProp.id) {
                window.location.href = `${window.location.href}/${product.id}`;
            }
        });

        ProductFacadeService.productUpdated$.subscribe(product => {
            if (product.id && product.id !== DefaultProductCategoryProp.id) {
                toaster.notify(`Product ${product.title} updated successfully!`, infoOptions);
            }
        });

        ProductFacadeService.productDeleted$.subscribe((id) => {
            if (id) {
                toaster.notify(`Product deleted successfully!`, infoOptions);
                ProductFacadeService.dispatchGetAllProducts();
            }
        });

        ProductFacadeService.error$.subscribe(error => {
            App.handleError(error);
        });
    }

    private registerUploadToastrs() {
        const infoOptions = this.toastrInforOptions;
        UploadFacadeService.fileUploaded$.subscribe((file) => {
            if (file) {
                toaster.notify(`File ${file.name} uploaded successfully!`, infoOptions);
                ProductFacadeService.dispatchGetAllProducts();
            }
        });

        UploadFacadeService.error$.subscribe(error => {
            App.handleError(error);
        });
    }
}

export default App;
