import React from 'react';
import './breadcrumb.css';
import {BreadcrumbProp} from "../models/breadcrumb-prop";
import {Breadcrumb} from "react-bootstrap";

export class BreadcrumbComponent extends React.Component<BreadcrumbProp> {

    render() {
        return <Breadcrumb>
            {this.props.items.map(item => (
                <li key={item.title} className={'breadcrumb-item' + (item.active ? ' active' : '')}>
                    {item.active ? item.title : <a href={item.link}>{item.title}</a>}
                </li>
            ))}
        </Breadcrumb>
    }
}

export default BreadcrumbComponent;
