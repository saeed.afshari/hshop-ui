import React from 'react';
import {render} from '@testing-library/react';
import BreadcrumbComponent from "./breadcrumb";

test('BreadcrumbComponent should be truthy', async () => {
  const {container} = render(<BreadcrumbComponent items={[]}/>);
  expect(container).toBeTruthy();
});
