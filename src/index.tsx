import React from 'react';
import ReactDOM from 'react-dom';
import './index.css';
import * as serviceWorker from './serviceWorker';
import App from "./components/app/app";

import 'bootstrap/dist/css/bootstrap.min.css';
import 'toasted-notes/src/styles.css';

import config from 'react-global-configuration';

const localBaseAPI = 'http://0.0.0.0:8080/v1';

const configuration = {
    categoryByNameAPI: `${localBaseAPI}/category/name`,
    categoriesByLevelAPI: `${localBaseAPI}/category/level`,
    categoriesBaseAPI: `${localBaseAPI}/category`,

    productBaseAPI: `${localBaseAPI}/product`,

    uploadBaseAPI: `${localBaseAPI}/upload`
};
if (process.env.NODE_ENV === 'production') {
    config.set(configuration);
} else if (process.env.NODE_ENV === 'development') {
    config.set(configuration);
} else if (process.env.NODE_ENV === 'test') {
    config.set(configuration);
}

ReactDOM.render(
    <React.StrictMode>
        <App/>
    </React.StrictMode>,
    document.getElementById('root')
);

// If you want your app to work offline and load faster, you can change
// unregister() to register() below. Note this comes with some pitfalls.
// Learn more about service workers: https://bit.ly/CRA-PWA
serviceWorker.unregister();
